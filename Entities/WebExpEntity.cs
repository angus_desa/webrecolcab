﻿namespace Entities
{
    public  class WebExpEntity
    {
        public int Linea { get; set; }
        public int Raza { get; set; }
        public int Expediente { get; set; }
        public string NomCriador { get; set; }
        public string Inicio { get; set; }
        public string Fin { get; set; }
        public string NomEstablecimiento { get; set; }
        public string Localidad { get; set; }
        public string CodigoPostal { get; set; }
    }
}
