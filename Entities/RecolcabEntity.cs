﻿namespace Entities
{
    public class RecolcabEntity
    {
        public string codasoc { get; set; }
        public string cod_cab { get; set; }
        public string cod_sra { get; set; }
        public string nom_cab { get; set; }
        public string nom_cria { get; set; }
    }
}
