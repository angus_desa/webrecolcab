﻿namespace Entities
{
    public class CopropiedadesGridEntity
    {
        public int Linea { get; set; }
        public int CodProp { get; set; }
        public string NombreCodProp { get; set; }
    }
}
