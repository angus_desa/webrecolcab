﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class MensajeResultadoProcesoEntity
    {
        public string Mensaje { get; set; }
        public string Contenido { get; set; }
        public int Refresh { get; set; }
        public bool Error { get; set; }
        public int Id { get; set; }
    }
}
