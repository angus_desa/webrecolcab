﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CopropiedadesEntity
    {
        public string CodCriadorCop { get; set; }
        public string NombreCop { get; set; }
        public string CriadorNiembro { get; set; }
        public string CriadorNombreNiembro { get; set; }
    }
}
