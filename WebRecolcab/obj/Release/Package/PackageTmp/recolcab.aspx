﻿<%@ Page Title="Expedientes" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="recolcab.aspx.cs" Inherits="WebRecolcab.recolcab" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" ClientIDMode="Static">

    <script src="Scripts/jquery.alerts.js"></script>
    <script src="Scripts/recolcab.js"></script>
    <script src="/Scripts/jquery.blockUI.js"></script>
    <script src="/Scripts/blockUI.js"></script>
    <link href="ccs/grid.css" rel="stylesheet" />
    <link href="ccs/jquery.alerts.css" rel="stylesheet" />

    <div>
        <h2>Automatización proceso recolcab</h2>
    </div>

    <div class="boxWhite">

        <div id="filtro" runat="server">
            <table>
                <tr>
                    <td class="label-text" style="padding-right: 10PX">Archivo:</td>
                    <td id="lblArchivoNovedes" runat="server" style="width: 400px; border: 1px solid gray; background-color: white" class="form-control"></td>
                    <td style="padding-left: 5px">
                        <a id="btnLeerArchivoNovedades" runat="server" style="cursor: pointer; text-decoration: none; color: black" title="Buscar archivo"><span class="icon-magnifying-glass"></span></a>
                    </td>
                </tr>
            </table>
        </div>

        <div id="result" runat="server">
            <div style="max-height: 300px; width: 1067px; overflow-y: auto">

                <asp:GridView ID="gridResult" runat="server" HeaderStyle-CssClass="gridFixedHeader" AutoGenerateColumns="False" CssClass="grid" GridLines="None" OnRowDataBound="gvEmpleados_RowDataBound">
                    <Columns>
                        <asp:BoundField DataField="codasoc" HeaderText="Cód. Asoc.">
                            <HeaderStyle CssClass="gridTitulo" HorizontalAlign="Left" Wrap="true" Width="100" />
                            <ItemStyle CssClass="gridDetalle" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" Width="100"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="cod_cab" HeaderText="Cód. Cab.">
                            <HeaderStyle CssClass="gridTitulo" HorizontalAlign="Left" Wrap="true" Width="100" />
                            <ItemStyle CssClass="gridDetalle" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" Width="100"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="cod_sra" HeaderText="Cód. SRA">
                            <HeaderStyle CssClass="gridTitulo" HorizontalAlign="Left" Wrap="true" Width="100" />
                            <ItemStyle CssClass="gridDetalle" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" Width="100"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="nom_cab" HeaderText="Nom. Cab.">
                            <HeaderStyle CssClass="gridTitulo" HorizontalAlign="Left" Wrap="true" Width="350" />
                            <ItemStyle CssClass="gridDetalle" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" Width="350"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="nom_cria" HeaderText="Nom. Cria">
                            <HeaderStyle CssClass="gridTitulo" HorizontalAlign="Left" Wrap="true" Width="400" />
                            <ItemStyle CssClass="gridDetalle" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" Width="400"></ItemStyle>
                        </asp:BoundField>

                    </Columns>
                </asp:GridView>
            </div>

            <div style="margin-top: 10px">
                <table>
                    <tr>
                        <td style="padding-right: 5px">
                            <h4>Total procesados:</h4>
                        </td>
                        <td style="padding-right: 40px; color: darkblue">
                            <h4>
                                <asp:Label ID="lblTotalProcesados" runat="server" Text=""></asp:Label>
                            </h4>
                        </td>
                        <td style="padding-right: 5px">
                            <h4>Total nuevos:</h4>
                        </td>
                        <td style="color: green">
                            <h4>
                                <asp:Label ID="lblTotalNuevos" runat="server" Text=""></asp:Label>
                            </h4>
                        </td>
                    </tr>
                </table>


            </div>
        </div>

    </div>

    <div style="display: none">
        <asp:FileUpload ID="FileUploadNovedades" runat="server" ClientIDMode="Static" onchange="getArchivoNovedades(this)" Width="578px" />
        <asp:HiddenField ID="hfPlanillaNombre" runat="server" />
        <asp:Button ID="BtnMostrarGrilla" runat="server" OnClick="BtnMostrarGrilla_Click" Text="Button" />
    </div>

    <div id="divBtnFiltro" runat="server" style="padding: 5px 0px">
        <input id="btnProcesar" type="button" value="Procesar" class="btn btn-success btn" style="margin-right: 5px" />
        <asp:Button ID="bntVolver" runat="server" Text="Volver" CssClass="btn btn-secondary btn" OnClick="bntVolver_Click"></asp:Button>
    </div>

    <div id="divBtnCerrarGrid" runat="server" style="margin: 10px 0px">
        <asp:Button ID="btnCerrarGridResult" runat="server" Text="Cerrar" CssClass="btn btn-default btn" OnClick="btnCerrarGridResult_Click" />
    </div>

</asp:Content>
