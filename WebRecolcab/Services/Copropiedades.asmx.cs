﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services;
using ClosedXML.Excel;
using Entities;
using interfaz.Dao;
using System.Data;
using System.Data.SqlClient;
using Common;
using System.Diagnostics;

namespace WebRecolcab.Services
{
    /// <summary>
    /// Descripción breve de Copropiedades
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
     [System.Web.Script.Services.ScriptService]
    public class Copropiedades : WebService
    {

        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity LeerArchivo(string Archivo)
        {
            var proceso = EnumProcesos.Copropiedades;

            Log.Registrar("--> Inicio LeerArchivo()", proceso);

            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Mensaje = "Atención";

            try
            {
                Log.Registrar("Abrir archivo", proceso);
                int posComa = Archivo.IndexOf(',') + 1;
                string archivo = Archivo.Substring(posComa, (Archivo.Length - posComa));
                byte[] myBase64ret = Convert.FromBase64String(archivo);
                MemoryStream ms = new MemoryStream(myBase64ret);
                XLWorkbook wb = new XLWorkbook(ms);
                IXLWorksheet ws = wb.Worksheet(1);

                var lst = new List<CopropiedadesEntity>();
                var cont = 0;
                var firstRow = true;

                Log.Registrar("Comienza lectura del archivo", proceso);

                foreach (IXLRow row in ws.Rows())
                {
                    if (row.Cell(1).Value.ToString().Length > 0)
                    {
                        if (firstRow)
                        {
                            // controlar cabecera.
                        }
                        else
                        {
                            lst.Add(new CopropiedadesEntity
                            {
                                CodCriadorCop = row.Cell(1).Value.ToString(),
                                NombreCop = row.Cell(2).Value.ToString(),
                                CriadorNiembro = row.Cell(3).Value.ToString(),
                                CriadorNombreNiembro = row.Cell(4).Value.ToString(),
                            });

                            cont++;
                        }

                        firstRow = false;
                    }
                }

                Log.Registrar("Fin lectura del archivo", proceso);

                Session["registros"] = null;

                if (cont > 0)
                {
                    Session["registros"] = lst;
                }

                msj.Contenido = "<b>" + cont.ToString() + "</b> registros a procesar"; ;

            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Error = true;
                msj.Contenido = ex.ToString();

                Log.Registrar(ex.ToString(), proceso);
            }

            Log.Registrar("--> Fin LeerArchivo()", proceso);

            return msj;
        }


        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity ProcesarArchivo(string filename)
        {
            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Mensaje = "Atención";
            msj.Contenido = "Fin del proceso";

            var proceso = EnumProcesos.Copropiedades;

            Log.Registrar("--> Inicio ProcesarArchivo()", proceso);

            try
            {
                if (Session["registros"] != null)
                {
                    msj.Contenido = "Archivo procesaro";

                    var listOfCopropiedad = new List<CopropiedadesEntity>();
                    listOfCopropiedad = (List<CopropiedadesEntity>)Session["registros"];

                    // Abrimos la conexion
                    string sCon = new conection().GetConnection();
                    SqlConnection conn8 = new SqlConnection(sCon);
                    conn8.Open();

                    int idcab;

                    // Cabecera 
                    string queryInsert = "insert into dbo.Copropiedades_Cab (FechaInicioProceso,NombreArchivo) output INSERTED.IntId VALUES(@FechaInicioProceso,@NombreArchivo)";
                    Log.Registrar( queryInsert, proceso);

                    using (SqlCommand cmd = new SqlCommand(queryInsert, conn8))
                    {
                        cmd.Parameters.AddWithValue("@FechaInicioProceso", DateTime.Now);
                        cmd.Parameters.AddWithValue("@NombreArchivo", filename);

                        idcab = (int)cmd.ExecuteScalar();
                        Session.Add("IdProceso", idcab);

                        if (conn8.State == ConnectionState.Open)
                            conn8.Close();

                    }

                    // Detalle
                    if (conn8.State == ConnectionState.Closed)
                        conn8.Open();

                    string sCodCriadorCop = string.Empty;

                    SqlCommand cmd8 = new SqlCommand();
                    queryInsert = "insert into dbo.Copropiedades_Deta (intId,CodProp,NombreCodProp ) values (@intId,@CodProp,@NombreCodProp );";
                    Log.Registrar( queryInsert, proceso);

                    foreach (var copropiedad in listOfCopropiedad)
                    {
                        if (sCodCriadorCop != copropiedad.CodCriadorCop)
                        {
                            sCodCriadorCop = copropiedad.CodCriadorCop;
                            cmd8 = new SqlCommand(queryInsert, conn8);
                            SqlParameter parameter = new SqlParameter("@intId", SqlDbType.Int);
                            parameter.Value = idcab;

                            SqlParameter parameter1 = new SqlParameter("@CodProp", SqlDbType.NVarChar);
                            parameter1.Value = Convert.ToString(copropiedad.CodCriadorCop);

                            SqlParameter parameter2 = new SqlParameter("@NombreCodProp", SqlDbType.NVarChar);
                            parameter2.Value = Convert.ToString(copropiedad.NombreCop);

                            cmd8.Parameters.Add(parameter);
                            cmd8.Parameters.Add(parameter1);
                            cmd8.Parameters.Add(parameter2);

                            cmd8.CommandType = CommandType.Text;
                            cmd8.Connection = conn8;

                            cmd8.ExecuteNonQuery();
                            cmd8.Dispose();

                            if (copropiedad.CriadorNiembro.Length > 0)
                            {
                                cmd8 = new SqlCommand(queryInsert, conn8);
                                SqlParameter parameter3 = new SqlParameter("@intId", SqlDbType.Int);
                                parameter3.Value = idcab;

                                SqlParameter parameter4 = new SqlParameter("@CodProp", SqlDbType.NVarChar);
                                parameter4.Value = Convert.ToString(copropiedad.CriadorNiembro);

                                SqlParameter parameter5 = new SqlParameter("@NombreCodProp", SqlDbType.NVarChar);
                                parameter5.Value = Convert.ToString(copropiedad.CriadorNombreNiembro);

                                cmd8.Parameters.Add(parameter3);
                                cmd8.Parameters.Add(parameter4);
                                cmd8.Parameters.Add(parameter5);

                                cmd8.CommandType = CommandType.Text;
                                cmd8.Connection = conn8;

                                cmd8.ExecuteNonQuery();
                                cmd8.Dispose();
                            }
                        }
                    }

                    // Alta nuevos copropiedadades
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd9 = new SqlCommand();
                    queryInsert = "update dbo.Copropiedades_Deta set Nuevo = 'Si' where intLinea in (select intLinea from dbo.Copropiedades_Deta co left join dbo.Criador c on co.CodProp = c.Expediente where c.Expediente is null and co.intId = @id);";                   
                    Log.Registrar( queryInsert, proceso);

                    cmd9 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter6 = new SqlParameter("@id", SqlDbType.Int);
                    parameter6.Value = idcab;
                    cmd9.Parameters.Add(parameter6);
                    cmd9.CommandType = CommandType.Text;
                    cmd9.Connection = conn8;

                    cmd9.ExecuteNonQuery();
                    cmd9.Dispose();

                    // Insertando en criador
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd10 = new SqlCommand();

                    queryInsert = "insert dbo.Criador(Expediente,Nombre,Estado) select co.CodProp,co.NombreCodProp, 1 from dbo.Copropiedades_Deta co where co.intId = @id and co.Nuevo is not null;";
                    Log.Registrar(queryInsert, proceso);
                    cmd10 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter7 = new SqlParameter("@id", SqlDbType.Int);
                    parameter7.Value = idcab;
                    cmd10.Parameters.Add(parameter7);
                    cmd10.CommandType = CommandType.Text;
                    cmd10.Connection = conn8;

                    cmd10.ExecuteNonQuery();
                    cmd10.Dispose();


                    // Finalizar proceso
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd11 = new SqlCommand();

                    queryInsert = "update dbo.Copropiedades_Cab set FechaFinProceso = GETDATE() where intId = @id;";
                    Log.Registrar(queryInsert, proceso);
                    cmd11 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter8 = new SqlParameter("@id", SqlDbType.Int);
                    parameter8.Value = idcab;
                    cmd11.Parameters.Add(parameter8);
                    cmd11.CommandType = CommandType.Text;
                    cmd11.Connection = conn8;

                    cmd11.ExecuteNonQuery();
                    cmd11.Dispose();
                }
                else
                {
                    msj.Contenido = "Archivo vacío";
                }
            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Refresh = 0;
                msj.Contenido = ex.ToString();
            }

            Log.Registrar("--> Fin ProcesarArchivo()", proceso);

            return msj;
        }
    }
}
