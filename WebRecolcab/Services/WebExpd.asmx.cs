﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services;
using ClosedXML.Excel;
using Entities;
using interfaz.Dao;
using System.Data;
using System.Data.SqlClient;
using Common;


namespace WebRecolcab.Services
{
    /// <summary>
    /// Descripción breve de WebExpd
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class WebExpd : System.Web.Services.WebService
    {

        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity LeerArchivo(string Archivo)
        {
            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Mensaje = "Atención";
            try
            {
                char separador = Convert.ToChar(";");
                int posComa = Archivo.IndexOf(',') + 1;
                string archivo = Archivo.Substring(posComa, (Archivo.Length - posComa));
                byte[] myBase64ret = Convert.FromBase64String(archivo);
                MemoryStream ms = new MemoryStream(myBase64ret);

                var content = new StreamReader(ms);

                var lst = new List<WebExpEntity>();
                var cont = 0;

                string linea;
                while ( (linea = content.ReadLine()) !=null )
                {
                    string[] fila = linea.Split(separador);
                    lst.Add(new WebExpEntity()
                    {
                        Raza = Convert.ToInt32(fila[0]),
                        Expediente = Convert.ToInt32(fila[1]),
                        NomCriador = fila[2].ToString(),
                        Inicio = fila[6].ToString(),
                        Fin = fila[7].ToString(),
                        NomEstablecimiento = fila[8].ToString(),
                        Localidad = fila[9].ToString(),
                        CodigoPostal = fila[10].ToString()
                    });
                    cont++;
                }

                Session["registros"] = null;

                if (cont > 0)
                {
                    Session["registros"] = lst;
                }

                msj.Contenido = "<b>" + cont.ToString() + "</b> registros a procesar"; ;

            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Error = true;
                msj.Contenido = ex.ToString();
            }

            return msj;
        }


        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity ProcesarArchivo(string filename)
        {
            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Mensaje = "Atención";
            msj.Contenido = "Fin del proceso";

            var proceso = EnumProcesos.Expedientes;

            Log.Registrar("Inicio WebExp " + DateTime.Now.ToString(), proceso);

            try
            {
                if (Session["registros"] != null)
                {
                    msj.Contenido = "Archivo procesaro";
                    string queryInsert;
                    int id  = 1;
                    var qry = string.Empty;
                    SqlCommand cmd = new SqlCommand();
                    SqlParameter param = new SqlParameter();

                    var lstExpd = new List<WebExpEntity>();
                    lstExpd = (List<WebExpEntity>)Session["registros"];

                    // Abrimos la conexion
                    string sCon = new conection().GetConnection();
                    SqlConnection conn8 = new SqlConnection(sCon);
                    conn8.Open();

                    Log.Registrar("Cabecera: insert into dbo.Expd_Cab " + DateTime.Now.ToString(), proceso);

                    // Cabecera 
                    int idcab;
                    using (cmd = new SqlCommand("insert into dbo.Expd_Cab (FechaInicioProceso,NombreArchivo) output INSERTED.IntId VALUES(@FechaInicioProceso,@NombreArchivo)", conn8))
                    {
                        cmd.Parameters.AddWithValue("@FechaInicioProceso", DateTime.Now);
                        cmd.Parameters.AddWithValue("@NombreArchivo", filename);

                        idcab = (int)cmd.ExecuteScalar();
                        Session.Add("IdProceso", idcab);

                        if (conn8.State == ConnectionState.Open)
                            conn8.Close();
                    }

                    if (conn8.State == ConnectionState.Closed)
                        conn8.Open();

                    SqlCommand cmd8 = new SqlCommand();
                    queryInsert = "insert into dbo.Expd_Deta (intId,Raza,Expediente,NomCriador, Inicio, Fin,NomEstablecimiento,Localidad,codigoPostal ) values (@intId,@Raza,@Expediente,@NomCriador, @Inicio, @Fin,@NomEstablecimiento,@Localidad,@codigoPostal);";

                    Log.Registrar("Alta de detalle Expd: insert into dbo.Expd_Deta " + DateTime.Now.ToString(), proceso);

                    //alta de detalle EXPD
                    foreach (var item in lstExpd)
                    {
                        cmd8 = new SqlCommand(queryInsert, conn8);
                        SqlParameter parameter = new SqlParameter("@intId", SqlDbType.Int);
                        parameter.Value = idcab;

                        SqlParameter parameter1 = new SqlParameter("@Raza", SqlDbType.NVarChar);
                        parameter1.Value = item.Raza;

                        SqlParameter parameter2 = new SqlParameter("@Expediente", SqlDbType.Int);
                        parameter2.Value = item.Expediente;

                        SqlParameter parameter3 = new SqlParameter("@NomCriador", SqlDbType.NVarChar);
                        parameter3.Value = item.NomCriador;

                        SqlParameter parameter4 = new SqlParameter("@Inicio", SqlDbType.NVarChar);
                        parameter4.Value = item.Inicio;

                        SqlParameter parameter5 = new SqlParameter("@Fin", SqlDbType.NVarChar);
                        parameter5.Value = item.Fin;

                        SqlParameter parameter6 = new SqlParameter("@NomEstablecimiento", SqlDbType.NVarChar);
                        parameter6.Value = item.NomEstablecimiento;

                        SqlParameter parameter7 = new SqlParameter("@Localidad", SqlDbType.NVarChar);
                        parameter7.Value = item.Localidad;

                        SqlParameter parameter8 = new SqlParameter("@codigoPostal", SqlDbType.Int);
                        parameter8.Value = item.CodigoPostal;

                        cmd8.Parameters.Add(parameter);
                        cmd8.Parameters.Add(parameter1);
                        cmd8.Parameters.Add(parameter2);
                        cmd8.Parameters.Add(parameter3);
                        cmd8.Parameters.Add(parameter4);
                        cmd8.Parameters.Add(parameter5);
                        cmd8.Parameters.Add(parameter6);
                        cmd8.Parameters.Add(parameter7);
                        cmd8.Parameters.Add(parameter8);

                        cmd8.CommandType = CommandType.Text;
                        cmd8.Connection = conn8;

                        cmd8.ExecuteNonQuery();
                        cmd8.Dispose();

                        id++;
                    }

                    //alta nuevos ExpD
                    Log.Registrar("Alta nuevos Expd: update dbo.Expd_Deta " + DateTime.Now.ToString(), proceso);
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd9 = new SqlCommand();
                    queryInsert = "update dbo.Expd_Deta set Nuevo = 'Si' where intLinea in (select intLinea from dbo.Expd_Deta co left join dbo.Criador c on co.Expediente = c.Expediente where c.Expediente is null and co.intId = @id); ";
                    cmd9 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter10 = new SqlParameter("@id", SqlDbType.Int);
                    parameter10.Value = idcab;
                    cmd9.Parameters.Add(parameter10);
                    cmd9.CommandType = CommandType.Text;
                    cmd9.Connection = conn8;

                    cmd9.ExecuteNonQuery();
                    cmd9.Dispose();

                    //alta nuevos criadores
                    Log.Registrar("Alta nuevos Criadores: exec [dbo].[p_ANG_ProcesarNovedadesEXP] @intid; " + DateTime.Now.ToString(), proceso);
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd12 = new SqlCommand();
                    queryInsert = "exec [dbo].[p_ANG_ProcesarNovedadesEXP] @intid; ";
                    cmd9 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter12 = new SqlParameter("@intid", SqlDbType.Int);
                    parameter12.Value = idcab;
                    cmd9.Parameters.Add(parameter12);
                    cmd9.CommandType = CommandType.Text;
                    cmd9.Connection = conn8;

                    cmd9.ExecuteNonQuery();
                    cmd9.Dispose();

                    //finalizar proceso
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd11 = new SqlCommand();

                    Log.Registrar("update dbo.Expd_Cab " + DateTime.Now.ToString(), proceso);
                    queryInsert = " update dbo.Expd_Cab set FechaFinProceso = GETDATE() where intId = @id; ";
                    cmd11 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter9 = new SqlParameter("@id", SqlDbType.Int);
                    parameter9.Value = idcab;
                    cmd11.Parameters.Add(parameter9);
                    cmd11.CommandType = CommandType.Text;
                    cmd11.Connection = conn8;

                    cmd11.ExecuteNonQuery();
                    cmd11.Dispose();

                    Log.Registrar("p_ANG_ConsWebEXPDProcesados " + DateTime.Now.ToString(), proceso);
                    qry = "exec p_ANG_ConsWebEXPDProcesados @Id; ";
                    param = new SqlParameter("@Id", SqlDbType.Int);
                    param.Value = idcab;
                    DataSet ds = new DataSet();
                    cmd = new SqlCommand(qry);
                    cmd.Parameters.Add(param);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.SelectCommand.Connection = conn8;
                    da.Fill(ds);

                    if (conn8.State == System.Data.ConnectionState.Open)
                        conn8.Close();
                }
            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Refresh = 0;
                msj.Contenido = ex.ToString();
                Log.Registrar(ex.ToString() + " " + DateTime.Now.ToString(), proceso);
            }
            Log.Registrar("Fin WebExp " + DateTime.Now.ToString(), proceso);
            return msj;
        }
    }
}
