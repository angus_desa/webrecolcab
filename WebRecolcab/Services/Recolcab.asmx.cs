﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Services;
using ClosedXML.Excel;
using Entities;
using interfaz.Dao;
using System.Data;
using System.Data.SqlClient;


namespace WebRecolcab.Services
{
    /// <summary>
    /// Descripción breve de Relcodcab
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class Relcodcab : WebService
    {

        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity LeerArchivo(string Archivo)
        {
            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Mensaje = "Atención";
            try
            {
                int posComa = Archivo.IndexOf(',') + 1;
                string archivo = Archivo.Substring(posComa, (Archivo.Length - posComa));
                byte[] myBase64ret = Convert.FromBase64String(archivo);
                MemoryStream ms = new MemoryStream(myBase64ret);
                XLWorkbook wb = new XLWorkbook(ms);
                IXLWorksheet ws = wb.Worksheet(1);

                var lst = new List<RecolcabEntity>();
                var cont = 0;
                var firstRow = true;

                foreach (IXLRow row in ws.Rows())
                {
                    if (row.Cell(1).Value.ToString().Length > 0)
                    {
                        if (firstRow)
                        {
                            // controlar cabecera.
                        }
                        else
                        {
                            lst.Add(new RecolcabEntity
                            {
                                codasoc = row.Cell(1).Value.ToString().Trim().Length == 0 ? "0" : row.Cell(1).Value.ToString().Trim(),
                                cod_cab = row.Cell(2).Value.ToString().Trim(),
                                cod_sra = row.Cell(3).Value.ToString(),
                                nom_cab = row.Cell(4).Value.ToString().Trim(),
                                nom_cria = row.Cell(5).Value.ToString().Trim()
                            });

                            cont++;
                        }

                        firstRow = false;
                    }
                }

                Session["registros"] = null;

                if (cont > 0)
                {
                    Session["registros"] = lst;
                }

                msj.Contenido = "<b>" + cont.ToString() + "</b> registros a procesar"; ;

            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Error = true;
                msj.Contenido = ex.ToString();
            }

            return msj;
        }

        [WebMethod(enableSession: true)]
        public MensajeResultadoProcesoEntity ProcesarArchivo(string filename)
        {
            var msj = new MensajeResultadoProcesoEntity();
            msj.Error = false;
            msj.Id = 0;
            msj.Mensaje = "Atención";
            msj.Contenido = "Fin del proceso";

            try
            {
                if (Session["registros"] != null)
                {
                    msj.Contenido = "Archivo procesaro";

                    var lstRelcodcab = new List<RecolcabEntity>();
                    lstRelcodcab = (List<RecolcabEntity>)Session["registros"];

                    // Abrimos la conexion
                    string sCon = new conection().GetConnection();
                    SqlConnection conn8 = new SqlConnection(sCon);
                    conn8.Open();

                    int idcab;
                    //// Cabecera 
                    using (SqlCommand cmd = new SqlCommand("insert into dbo.RelcodCab_Cab (CreateTime) output INSERTED.ID VALUES(@CreateTime)", conn8))
                    {
                        cmd.Parameters.AddWithValue("@CreateTime", DateTime.Now);

                        idcab = (int)cmd.ExecuteScalar();
                        msj.Id = idcab;
                        Session.Add("IdProceso", idcab);

                        if (conn8.State == System.Data.ConnectionState.Open)
                            conn8.Close();
                    }

                    //Detalle
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();

                    SqlCommand cmd8 = new SqlCommand();
                    string queryInsert = "insert into dbo.RelcodCab_Deta (id,codasoc,cod_cab,cod_sra,nom_cab,nom_cria ) values (@id,@codasoc,@cod_cab,@cod_sra,@nom_cab,@nom_cria );";
                    foreach (var Criador in lstRelcodcab)
                    {

                        cmd8 = new SqlCommand(queryInsert, conn8);
                        SqlParameter parameter = new SqlParameter("@id", SqlDbType.Int);
                        parameter.Value = idcab;

                        SqlParameter parameter1 = new SqlParameter("@codasoc", SqlDbType.Int);
                        parameter1.Value = Convert.ToInt32(Criador.codasoc);

                        SqlParameter parameter2 = new SqlParameter("@cod_cab", SqlDbType.Int);
                        parameter2.Value = Convert.ToInt32(Criador.cod_cab);

                        SqlParameter parameter3 = new SqlParameter("@cod_sra", SqlDbType.Int);
                        parameter3.Value = Criador.cod_sra.Length == 0 ? 0 : Convert.ToInt32(Criador.cod_sra);

                        SqlParameter parameter4 = new SqlParameter("@nom_cab", SqlDbType.VarChar);
                        parameter4.Value = Criador.nom_cab;
                        SqlParameter parameter5 = new SqlParameter("@nom_cria", SqlDbType.VarChar);
                        parameter5.Value = Criador.nom_cria;

                        cmd8.Parameters.Add(parameter);
                        cmd8.Parameters.Add(parameter1);
                        cmd8.Parameters.Add(parameter2);
                        cmd8.Parameters.Add(parameter3);
                        cmd8.Parameters.Add(parameter4);
                        cmd8.Parameters.Add(parameter5);

                        cmd8.CommandType = CommandType.Text;
                        cmd8.Connection = conn8;

                        cmd8.ExecuteNonQuery();
                        cmd8.Dispose();
                    }

                    //alta nuevos criadores
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd9 = new SqlCommand();
                    queryInsert = "exec [dbo].[p_ANG_ProcesarNovedadesRelcodCab] @intid; ";
                    cmd9 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter12 = new SqlParameter("@intid", SqlDbType.Int);
                    parameter12.Value = idcab;
                    cmd9.Parameters.Add(parameter12);
                    cmd9.CommandType = CommandType.Text;
                    cmd9.Connection = conn8;

                    cmd9.ExecuteNonQuery();
                    cmd9.Dispose();

                    //finalizar proceso
                    if (conn8.State == System.Data.ConnectionState.Closed)
                        conn8.Open();
                    SqlCommand cmd11 = new SqlCommand();

                    queryInsert = " update dbo.RelcodCab_Cab set FinalizeTime = GETDATE() where ID = @id; ";
                    cmd11 = new SqlCommand(queryInsert, conn8);
                    SqlParameter parameter9 = new SqlParameter("@id", SqlDbType.Int);
                    parameter9.Value = idcab;
                    cmd11.Parameters.Add(parameter9);
                    cmd11.CommandType = CommandType.Text;
                    cmd11.Connection = conn8;

                    cmd11.ExecuteNonQuery();
                    cmd11.Dispose();

                    if (conn8.State == ConnectionState.Open)
                        conn8.Close();
                }
            }
            catch (Exception ex)
            {
                msj.Mensaje = "Error";
                msj.Refresh = 0;
                msj.Contenido = ex.ToString();
            }

            return msj;
        }
    }

}
