﻿$(document).ready(function () {
    // jquery.blockUI.js, debe existir y estar cargado en el sitio.
    // Bloqueo ($.blockUI) la pantalla cuando inicia la petición ajax (ajaxStart) y 
    // Desbloqueo ($.unblockUI) la pantalla cuando termina la petición ajax (ajaxStop).
    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
});


// Funciones que pueden ser llamadas desde el cs de la página para procesos largos del lado del server.
// Ejemplo:             
//      Inicia el bloqueo
//         1. ScriptManager.RegisterStartupScript(this, typeof (Page), "bloquear", "blockUI()", true);
//         2. procedo dentro del método del cs....
//      Fin del bloque
//         3. ScriptManager.RegisterStartupScript(this, typeof(Page), "desbloquear", "unblockUI()", true);
function blockUI() {
    $.blockUI();
}

function unblockUI() {
    $.unblockUI();
}
