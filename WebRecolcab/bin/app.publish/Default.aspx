﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebRecolcab._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Consola</h1>
        <p class="lead">Herramienta de testeo de procesos automatizados.</p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Recolcab</h2>
            <p>
               Automatización proceso recolcab.
            </p>
            <p>
                <a class="btn btn-primary btn" href="recolcab.aspx">Procesar &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Expedientes </h2>
            <p>
                Actualización de novedades de expedientes SRA.
            </p>
            <p>
                <a class="btn btn-primary btn" href="webexpd.aspx">Procesar &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Copropiedades</h2>
            <p>
                Actualización de copropiedades.
            </p>
            <p>
                <a class="btn btn-primary btn" href="WebCopropiedades.aspx">Procesar &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
