﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebRecolcab.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Su contacto.</h3>
    <address>
        We agile it<br />
        Serâ un gesto hablar con ud.<br />
        <abbr title="Celular">Celular:</abbr>
        15 56624615
    </address>

    <address>
        <strong>Soporte têcnico:</strong>   <a href="mailto:martin.farias@gmail.com">martin.farias@gmail.com</a><br />
   
    </address>
</asp:Content>
