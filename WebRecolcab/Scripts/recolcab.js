﻿function attachEventsJs() {

    $("#btnLeerArchivoNovedades").click(function () {
        document.getElementById("FileUploadNovedades").click();
    });

    if ($("#hfPlanillaNombre").val() == "") {
        $("#btnProcesar").hide();
        $("#result").hide();
        $("#divBtnCerrarGrid").hide();
        $("#filtro").show();
    };

    $("#btnProcesar").click(function () {
        jConfirm('¿ Seguro desea procesar el archivo ' + $("#hfPlanillaNombre").val() + ' ?', 'Responder', function (r) {
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: "/Services/Recolcab.asmx/ProcesarArchivo",
                    contentType: "application/json; charset=utf-8",
                    data: "{'filename': '" + $("#hfPlanillaNombre").val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (!data.d.Error) {
                            $("#filtro").hide();
                            $("#divBtnFiltro").hide();
                            $("#result").show();
                            $("#divBtnCerrarGrid").show();
                            $("#BtnMostrarGrilla").click();
                        }
                        else {
                            jAlert(data.d.Contenido, data.d.Mensaje);
                        }
                    },
                    error: errorAjax
                });
            }
        });
    });
}

function cerrarGrid() {
    $("#result").hide();
    $("#divBtnCerrarGrid").hide();
    $("#btnProcesar").hide();
    $("#filtro").show();
    $("divBtnFiltro#").show();
}

function getArchivoNovedades(valor) {
    if (valor.value != "") {
        if (valor.files && valor.files[0]) {
            var nameFile = valor.files[0].name;
            var sizeFile = valor.files[0].size;
            var typeFile = valor.files[0].type;
            var allowedExtensions = /(.xls|.xlsx)$/i;
            if (!allowedExtensions.exec(valor.files[0].name)) {
                jAlert('Sólo archivo Excel');
                valor.value = '';
                $("#btnUpload").hide();
                $("#hfPlanillaNombre").val('');
                $("#lblArchivoNovedes").text('');
                return false;
            }

            // Leer archivo.
            var reader = new FileReader();
            reader.onload = function (e) {
                $.ajax({
                    type: "POST",
                    url: "/Services/Recolcab.asmx/LeerArchivo",
                    contentType: "application/json; charset=utf-8",
                    data: "{'Archivo': '" + e.target.result.toString() + "'}",
                    dataType: "json",
                    success: function (data) {
                        jAlert(data.d.Contenido, data.d.Mensaje);
                        if (!data.d.Error) {
                            $("#btnProcesar").show();
                        }
                        else {
                            $("#btnProcesar").hide();
                        }
                    },
                    error: function (data) {
                        $("#btnProcesar").hide();
                        errorAjax;
                    }
                });
            }
            reader.readAsDataURL(valor.files[0]);

            $("#lblArchivoNovedes").text(nameFile);
            $("#btnProcesar").show();
            $("#hfPlanillaNombre").val(nameFile);
        }
    }
}

function errorAjax(jqXHR, textStatus, errorThrow) {
    var exInfo = jQuery.parseJSON(jqXHR.responseText);
    alert("Error en Servicio\n" + errorThrow + "\n" + exInfo.Message);
}
