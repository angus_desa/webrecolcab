﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using interfaz.Dao;
using System.Web.UI.WebControls;
using Entities;
using System.Windows;

namespace WebRecolcab
{
    public partial class recolcab : Page
    {
        protected  void Page_Load( object sender, EventArgs e)
        {
            AttachEventsJs();
        }

        private void AttachEventsJs()
        {
            ScriptManager.RegisterStartupScript(this, typeof(Page), "", "attachEventsJs();", true);
        }

        protected void gvEmpleados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Al tener en el Grid la fila de títulos fija, hay que modificar la primer fila de registros,
                // para que sea visible luego de los títulos.
                // Si se cambia el valor de padding de la clase css .gridDetalle, se debe cambiar este valor también.
                if (e.Row.RowIndex == 0)
                {
                    int c = e.Row.Cells.Count;
                    for (int i = 0; i < c; i++)
                    {
                        e.Row.Cells[i].Style.Add("padding-top", "34px");
                    }
                }
                e.Row.Attributes.Add("onmouseover", "GridMouseEvents(this, event)");
                e.Row.Attributes.Add("onmouseout", "GridMouseEvents(this, event)");
            }
        }

        protected void bntVolver_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        protected void btnCerrarGridResult_Click(object sender, EventArgs e)
        {
            hfPlanillaNombre.Value = String.Empty;
            result.Visible = false;
            filtro.Visible = true;
            divBtnFiltro.Visible = true;
            divBtnCerrarGrid.Visible = false;
            Session["registros"] = null;
        }

        protected void BtnMostrarGrilla_Click(object sender, EventArgs e)
        {
            this.filtro.Visible = false;
            this.divBtnFiltro.Visible = false;

            lblTotalProcesados.Text = string.Empty;
            lblTotalNuevos.Text = string.Empty;

            string sCon = new conection().GetConnection();

            ////Abrimos la conexion
            SqlConnection conn = new SqlConnection(sCon);
            
            if (conn.State == ConnectionState.Closed)
                conn.Open();

            SqlCommand cmd = new SqlCommand("p_ANG_ConsRelcodcabProcesados");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@Id", (int)Session["IdProceso"]));      
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.SelectCommand.Connection = conn;
            da.Fill(ds);

            var lstResult = new List<RecolcabEntity>();

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        lstResult.Add(new RecolcabEntity()
                        {
                            codasoc = row["codasoc"].ToString(),
                            cod_cab = row["cod_cab"].ToString(),
                            cod_sra = row["cod_sra"].ToString(),
                            nom_cab = row["nom_cab"].ToString(),
                            nom_cria = row["nom_cria"].ToString()
                        });
                    }
                    gridResult.DataSource = lstResult;
                    gridResult.DataBind();
                }
                else
                {
                    gridVacio();
                }
                lblTotalProcesados.Text = ds.Tables[1].Rows[0]["Total"].ToString();
                lblTotalNuevos.Text = ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                gridVacio();

            }

            this.result.Visible = true;
            this.divBtnCerrarGrid.Visible = true;
        }

        private void gridVacio()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("codasoc");
            dt.Columns.Add("cod_cab");
            dt.Columns.Add("cod_sra");
            dt.Columns.Add("nom_cab");
            dt.Columns.Add("nom_cria");
            DataRow dr;
            dr = dt.NewRow();
            dr["codasoc"] = string.Empty;
            dr["cod_cab"] = string.Empty;
            dr["cod_sra"] = string.Empty;
            dr["nom_cab"] = string.Empty;
            dr["nom_cria"] = string.Empty;
            dt.Rows.Add(dr);
            gridResult.DataSource = dt;
            gridResult.DataBind();
        }
    }
}