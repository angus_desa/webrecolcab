﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace interfaz.Biz
{
    public class Expd
    {
        public string Raza { get; set; }
        public string Expediente { get; set; }
        public string NomCriador { get; set; }
        public string Inicio { get; set; }
        public string Fin { get; set; }
        public string NombEstablecimiento { get; set; }
        public string Localidad { get; set; }
        public string codigoPostal { get; set; }

        public Expd(string Raza, string Expediente, string NomCriador, string Inicio, string Fin, string NomEstablecimiento, string Localidad, string codigoPostal)
        {
            this.Raza = Raza;
            this.Expediente = Expediente;
            this.NomCriador = NomCriador;
            this.Inicio = Inicio;
            this.Fin = Fin;
            this.NombEstablecimiento = NomEstablecimiento;
            this.Localidad = Localidad;
            this.codigoPostal = codigoPostal;

        }
    }
}
