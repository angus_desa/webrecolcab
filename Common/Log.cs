﻿using System;
using System.IO;
using System.Configuration;

namespace Common
{
    public  class Log
    {
        public static void Registrar(string Msg, Enum Proceso)
        {
            string archivo = string.Empty;

            switch (Proceso)
            {
                case EnumProcesos.Copropiedades:
                    archivo = ConfigurationManager.AppSettings["FileLogCopropiedades"].ToString();
                    break;
                case EnumProcesos.Recolcab:
                    archivo = ConfigurationManager.AppSettings["FileLogRecolcab"].ToString();
                    break;
                case EnumProcesos.Expedientes:
                    archivo = ConfigurationManager.AppSettings["FileLogExpedientes"].ToString();
                    break;
            }

            TextWriter tw = new StreamWriter(archivo, true);

            tw.WriteLine(DateTime.Now.ToString() + " - " + Msg.Trim());

            tw.Close();
        }
    }
}
